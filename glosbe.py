import requests

import json

def get_translation(source_language, target_language, word_id):
    url = 'https://glosbe.com/gapi/translate?format=json&from=' \
            + source_language + '&dest=' + target_language + '&phrase=' \
            + word_id
    r = requests.get(url)

    output = set()
    if r.status_code == 200:
        for e in r.json()['tuc']:
            if 'phrase' in e.keys():
                output.add(e['phrase']['text'])

    return (r.status_code, output)
