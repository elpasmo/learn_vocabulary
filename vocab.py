import argparse
import os

from appdirs import user_data_dir
from glosbe import get_translation

_USER_DATA_DIR = user_data_dir('LearnVocabulary') + os.path.sep

def load_settings():
    settings = {}
    fname = _USER_DATA_DIR + 'settings'
    if not os.path.isfile(fname):
        configure()

    with open(fname, 'r') as settings_file:
        for line in settings_file:
            k, v = line.strip('\n').split('=')
            settings[k.strip()] = v.strip()

    return settings


def load_vocabulary(source, target):
    print(_USER_DATA_DIR + source + '_' + target)
    vocabulary = {}
    with open(_USER_DATA_DIR + source + '_' + target, 'r') as vocab_file:
        for line in vocab_file:
            n, v, d = line.strip('\n').split(':')
            vocabulary[v.strip()] = [int(n.strip()), d.split(', ')]

    return vocabulary


def record_learned_word(word):
    with open(_USER_DATA_DIR + 'learned.txt', 'a') as learned_file:
        learned_file.write(word + '\n')


def add_vocabulary(source, target, word):
    settings = load_settings()
    parameters = dict()
    parameters['source_language'] = source
    parameters['target_language'] = target
    parameters['word_id'] = word
    c, t = get_translation(**parameters)
    if c == 200:
        if len(t) > 0:
            trans = ', '.join(t)
            print('Added: {} meaning {}'.format(word, trans))
            with open(_USER_DATA_DIR + source + '_' + target, 'a') as output:
                output.write('0:{}:{}\n'.format(word, trans))
        else:
            print('Error adding word {}:'.format(word) \
                    +' no translations found. Try another variant.')
    else:
        print('Error adding word {}: {}'.format(word, c))


def check(source, target, number_tries, number_success):
    if number_tries == None or number_success == None:
        print('Settings not found. Please run again with configure command.')
        return

    i = 0
    vocabulary = load_vocabulary(source, target)
    words = set(vocabulary.keys())
    while i < int(number_tries) and len(words) > 0:
        word = words.pop()
        t = vocabulary[word][1][:]
        answer = input("What's the meaning of {word}? ".format(word=word))
        if answer.lower() in t:
            t.remove(answer.lower())
            if len(t) > 0:
                print('Right! Other meanings: {}'.format(', '.join(t)))
            else:
                print('Right!')
            vocabulary[word][0] += 1
            if vocabulary[word][0] == int(number_success):
                del vocabulary[word]
                record_learned_word(word)
        else:
            print('Wrong! It means: {}'.format(', '.join(t)))

        i += 1

    with open(_USER_DATA_DIR + source + '_' + target, 'w') as output:
        for k in vocabulary.keys():
            output.write('{}:{}:{}\n'.format(vocabulary[k][0], k, \
                    ', '.join(vocabulary[k][1])))


def configure():
    settings = dict()
    settings['default_source'] = \
            input("What's the default source language? ")
    settings['default_target'] = \
            input("What's the default target language? ")
    settings['number_success'] = \
            input("How many times do you need to succeed to learn a word? ")
    settings['number_tries'] = \
            input ("How many questions do you want the test to have? ")

    if not os.path.exists(_USER_DATA_DIR):
        os.mkdir(_USER_DATA_DIR)

    with open(_USER_DATA_DIR + 'settings', 'w') as output:
        for k in settings.keys():
            output.write('{}={}\n'.format(k, settings[k]))


if __name__ == '__main__':
    settings = load_settings()

    parser = argparse.ArgumentParser(description='Learn vocabulary.')
    subparsers = parser.add_subparsers(description='Sub-command help.')

    parser_add = subparsers.add_parser('add',
            help='Add command allows to add new vocabulary.')
    parser_add.add_argument('word', help='Word to add.')
    parser_add.add_argument('--source', '-s', default=settings.get('default_source'), \
            help='Source language.')
    parser_add.add_argument('--target', '-t', default=settings.get('default_target'), \
            help = 'Target language.')
    parser_add.set_defaults(func=add_vocabulary)

    parser_check = subparsers.add_parser('check',
            help='Check your vocabulary knowledge.')
    parser_check.add_argument('--source', '-s', default=settings.get('default_source'), \
            help='Source language.')
    parser_check.add_argument('--target', '-t', default=settings.get('default_target'), \
            help = 'Target language.')
    parser_check.set_defaults(func=check)

    parser_configure = subparsers.add_parser('configure',
            help='Configures the app.')
    parser_configure.set_defaults(func=configure)

    args = parser.parse_args()
    try:
        if args.func == configure:
            args.func()
        elif args.func == check:
            args.func(args.source, args.target, settings.get('number_tries'), \
                     settings.get('number_success'))
        elif args.func == add_vocabulary:
            args.func(args.source, args.target, args.word)
    except AttributeError:
       parser.print_help()
